import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * Handles Film objects and all methods to do with films
 *
 * @author Corey
 *
 */

public class Film {

  public static ArrayList<Film> filmList = new ArrayList<Film>();

  private int id;
  private String title;
  private int year;
  private String description;
  private String genre;
  private String actors;
  private String director;
  private double rating;
  private int stock;
  private double price;

  public static final int MAX_RATING = 5;

  private static Film film;

  public Film(Film film) {
    Film.film = film;
    this.id = film.id;
    this.title = film.title;
    this.year = film.year;
    this.description = film.description;
    this.genre = film.genre;
    this.actors = film.actors;
    this.director = film.director;
    this.rating = film.rating;
    this.stock = film.stock;
    this.price = film.price;
  }

  public Film(String title, int year, String description, String genre, String actors,
      String director, double rating) {
    this.title = title;
    this.year = year;
    this.description = description;
    this.genre = genre;
    this.actors = actors;
    this.director = director;
    this.rating = rating;
  }

  public Film(int id, String title, int year, String description, String genre, String actors,
      String director, double rating, double price, int stock) {
    this.id = id;
    this.title = title;
    this.year = year;
    this.description = description;
    this.genre = genre;
    this.actors = actors;
    this.director = director;
    this.rating = rating;
    this.price = price;
    this.stock = stock;
  }

  public Film getFilm() {
    return film;
  }

  public int getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public int getYear() {
    return year;
  }

  public String getDescription() {
    return description;
  }

  public String getGenre() {
    return genre;
  }

  public String getActors() {
    return actors;
  }

  public String getDirector() {
    return director;
  }

  public double getRating() {
    return rating;
  }

  public double getPrice() {
    return price;
  }

  public int getStock() {
    return stock;
  }

  /**
   * Returns a Film object based on the given id
   *
   * @param id - the id of the film being returned
   * @return a Film object with a primary key of <code>id</code>
   */
  public static Film getFilm(int id) {
    for (int i = 0; i < filmList.size(); i++) {
      if (filmList.get(i).getId() == id) {
        return new Film(filmList.get(i));
      }
    }
    return null;
  }

  public static Film getFilm(String id) {
    int id2 = Integer.parseInt(id.trim());
    for (int i = 0; i < filmList.size(); i++) {
      if (filmList.get(i).getId() == id2) {
        return new Film(filmList.get(i));
      }
    }
    return null;
  }

  /**
   * Calculates the rating based on given numbers
   *
   * @param rating - the existing rating
   * @param newRating - the rating given
   * @param timesRated - amount of times it has been rated
   * @return new average rating
   */
  public double calcRating(double rating, int newRating, int timesRated) {
    timesRated++;
    rating = (rating + newRating) / timesRated;
    if (rating >= MAX_RATING) {
      rating = MAX_RATING;
    }

    return rating;

  }

  /**
   * Prints all the fields contained within a Film object
   */
  void printInfo() {
    System.out.println(id + " \n" + title + " \n" + year + " \n" + genre + " \n" + description
        + " \n" + actors.toString() + " \n" + director + " \n" + rating + " \n");
  }

  /**
   * Gets all the films from the database and puts them into an ArrayList
   *
   * @param window - Which window pop ups display
   * @param notify - Whether you want popup notifications appearing
   * @throws SQLException
   */
  public static void retrieveFilms(Component window, boolean notify) throws SQLException {

    String sqlAddress = SQL.sqlAddress;
    String sqlTable = SQL.sqlTable;
    String sqlUsername = SQL.sqlUsername;
    String sqlPassword = SQL.sqlPassword;
    String sqlDatabaseLocation = sqlAddress + "/" + sqlTable;

    SQL database = new SQL(sqlUsername, sqlPassword, sqlDatabaseLocation, 1);
    database.initialise();

    try {
      filmList.removeAll(filmList);
      ResultSet query = database.executeQuery("SELECT * from movies");

      while (query.next()) {
        if (query != null) {
          filmList.add(new Film(query.getInt("id"), query.getString("title"), query.getInt("year"),
              query.getString("description"), query.getString("genre"), query.getString("actor"),
              query.getString("director"), query.getDouble("rating"), query.getDouble("price"),
              query.getInt("instock")));
          if (notify) {
            Film f = new Film(getFilm(query.getInt("ID")));
            f.printInfo();
          }
        }
      }
      if (notify)
        JOptionPane.showMessageDialog(window, filmList.size() + " films successfully retrieved.");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    System.out.println(filmList.size() + " films retrieved.");
    database.shutdown();
  }
}
