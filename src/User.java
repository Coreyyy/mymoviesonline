import java.awt.Component;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;

/**
 * Handles User objects and all methods to do with users
 *
 * @author Corey
 *
 */

public class User {

  private static final Random RANDOM = new SecureRandom();

  public static ArrayList<User> userList = new ArrayList<User>();

  private int id;
  private String username;
  private String password;
  private String salt;
  private String email;
  private boolean admin;

  private static User user;

  public User(int id, String username, boolean admin) {
    this.id = id;
    this.username = username;
    this.admin = admin;
  }

  public User(String username, String password, String salt, String email) {
    this.username = username;
    this.password = password;
    this.salt = salt;
    this.email = email;
  }

  public User(int id, String username, String password, String salt, String email, boolean admin) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.salt = salt;
    this.email = email;
    this.admin = admin;
  }

  public User(User user) {
    User.user = user;
    this.id = user.id;
    this.username = user.username;
    this.password = user.password;
    this.salt = user.salt;
    this.email = user.email;
    this.admin = user.admin;
  }

  public int getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getSalt() {
    return salt;
  }

  public String getEmail() {
    return email;
  }

  public boolean getAdmin() {
    return admin;
  }

  public static User getUser(int id) {
    for (int i = 0; i < userList.size(); i++) {
      if (userList.get(i).getId() == id) {
        return new User(userList.get(i));
      }
    }
    return null;
  }

  public static User getUser(String id) {
    int id2 = Integer.parseInt(id.trim());
    for (int i = 0; i < userList.size(); i++) {
      if (userList.get(i).getId() == id2) {
        return new User(userList.get(i));
      }
    }
    return null;
  }

  /**
   * Prints all the fields contained within a User object
   */
  void printInfo() {
    System.out.println(id + " \n" + username + " \n" + password + " \n" + salt + " \n" + email
        + " \n" + admin + " \n");
  }

  /**
   * Gets all the users from the database and puts them into an ArrayList
   *
   * @param window - Which window pop ups display
   * @param notify - Whether you want popup notifications appearing
   * @throws SQLException
   */
  public static void retrieveUsers(Component window, boolean notify) throws SQLException {

    String sqlAddress = SQL.sqlAddress;
    String sqlTable = SQL.sqlTable;
    String sqlUsername = SQL.sqlUsername;
    String sqlPassword = SQL.sqlPassword;
    String sqlDatabaseLocation = sqlAddress + "/" + sqlTable;

    SQL database = new SQL(sqlUsername, sqlPassword, sqlDatabaseLocation, 1);
    database.initialise();

    try {
      userList.removeAll(userList);
      ResultSet query = database.executeQuery("SELECT * from users");

      while (query.next()) {
        if (query != null) {
          userList.add(new User(query.getInt("ID"), query.getString("username"),
              query.getString("password"), query.getString("salt"), query.getString("email"),
              query.getInt("admin") == 1 ? true : false));
          if (notify) {
            User u = new User(getUser(query.getInt("ID")));
            u.printInfo();
          }
        }
      }
      if (notify)
        JOptionPane.showMessageDialog(window, userList.size() + " users successfully retrieved.");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    System.out.println(userList.size() + " users retrieved.");
    database.shutdown();
  }

  /**
   * Generates a random password
   *
   * @return 10 digit random character password
   */
  public static String generatePassword() {
    String chars =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890/ -+=.:;@?!#'&%$�![]()*";
    String pw = "";

    for (int i = 0; i < 16; i++) {
      pw += chars.charAt((int) (Math.random() * chars.length()));
    }
    return pw;
  }

  /**
   * Hashes a password
   *
   * @param salt Randomly generated salt to use in the hashing
   * @param input The password to hash
   * @return A hashed password to get high with
   * @throws Exception
   */
  public static String hash(String input, String salt) throws Exception {
    try {
      byte[] pw;
      MessageDigest digest = MessageDigest.getInstance("SHA-256");
      pw = digest.digest((input + salt).getBytes("UTF-8"));
      return byteToHex(pw);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
      return null;
    }
  }

  /**
   * Converts bytes into readable hex format
   *
   * @param b - The byte array to convert
   * @return Readable Hex string
   */
  public static String byteToHex(byte[] b) {
    String result = "";
    for (int i = 0; i < b.length; i++) {
      result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
    }
    return result;
  }

  /**
   * Returns a random salt to be used to hash a password.
   *
   * @return a 16 bytes random salt
   */
  public static byte[] getNextSalt() {
    byte[] salt = new byte[16];
    RANDOM.nextBytes(salt);
    return salt;
  }
}
