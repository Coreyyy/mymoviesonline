
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {


  public static void send(User user) throws UnsupportedEncodingException {
    Properties props = new Properties();
    Session session = Session.getDefaultInstance(props, null);

    String msgBody = "You have had an account made for you at MyMoviesOnline.co.uk";

    try {


      // TODO finish this

      Message msg = new MimeMessage(session);
      // props.setProperty(key, value)
      msg.setFrom(new InternetAddress("admin@mymoviesonline.co.uk", "MyMoviesOnline"));
      msg.addRecipient(Message.RecipientType.TO,
          new InternetAddress(user.getEmail(), user.getUsername()));
      msg.setSubject("MyMoviesOnline");
      msg.setText(msgBody);
      Transport.send(msg);
      System.out.println("User " + user.getUsername() + " emailed about account changes.");

    } catch (AddressException e) {
      System.out.println(e);
    } catch (MessagingException e2) {
      System.out.println(e2);
    }
  }

}
