import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class Login {

  private static JFrame frmLogin;
  private static JPasswordField passwordField;
  private static JLabel lblUsername;
  private static JLabel lblPassword;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          @SuppressWarnings("unused")
          Login window = new Login();
          Login.frmLogin.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public Login() {
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  static void initialize() {
    frmLogin = new JFrame();
    frmLogin.setIconImage(
        Toolkit.getDefaultToolkit().getImage("E:\\Computing\\COMP 4\\Java\\Company Logo.png"));
    frmLogin.setTitle("Login");
    frmLogin.setBounds(100, 100, 450, 300);
    frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frmLogin.getContentPane().setLayout(null);
    frmLogin.setVisible(true);

    JFormattedTextField textArea = new JFormattedTextField();
    textArea.setBounds(117, 44, 191, 29);
    frmLogin.getContentPane().add(textArea);

    passwordField = new JPasswordField();
    passwordField.setBounds(117, 85, 191, 29);
    frmLogin.getContentPane().add(passwordField);

    lblUsername = new JLabel("Username");
    lblUsername.setBounds(55, 44, 63, 29);
    frmLogin.getContentPane().add(lblUsername);

    lblPassword = new JLabel("Password");
    lblPassword.setBounds(55, 85, 63, 29);
    frmLogin.getContentPane().add(lblPassword);

    JButton btnLogin = new JButton("Login");
    btnLogin.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        login(textArea.getText(), new String(passwordField.getPassword()));
      }
    });
    btnLogin.setBounds(117, 127, 97, 25);
    frmLogin.getContentPane().add(btnLogin);

    JButton btnExit = new JButton("Exit");
    btnExit.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
      }
    });
    btnExit.setBounds(211, 127, 97, 25);
    frmLogin.getContentPane().add(btnExit);

    JButton btnHackTheSystem = new JButton("Hack the system");
    btnHackTheSystem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {

        JOptionPane.showMessageDialog(frmLogin, "Login successful");
        Frame.initialize();
        frmLogin.dispose();

      }
    });
    btnHackTheSystem.setBounds(92, 191, 134, 25);
    frmLogin.getContentPane().add(btnHackTheSystem);
  }

  private static void login(String username, String password) {

    if (username.trim().equals("") || password.trim().equals("")) {
      JOptionPane.showMessageDialog(frmLogin, "Username/Password cannot be blank");
      return;
    }

    String sqlAddress = SQL.sqlAddress;
    String sqlTable = SQL.sqlTable;
    String sqlUsername = SQL.sqlUsername;
    String sqlPassword = SQL.sqlPassword;
    String sqlDatabaseLocation = sqlAddress + "/" + sqlTable;

    SQL database = new SQL(sqlUsername, sqlPassword, sqlDatabaseLocation, 1);
    database.initialise();

    try {
      ResultSet query = database.executeQuery(
          "SELECT username, password, salt from users WHERE username = '" + username + "'");
      query.next();

      String resultUsername = query.getString("username");
      String resultPassword = query.getString("password");
      String resultSalt = query.getString("salt");

      if (query != null && resultUsername.equalsIgnoreCase(username)
          && hash(resultSalt, password).equals(resultPassword)) {

        JOptionPane.showMessageDialog(frmLogin, "Login successful");
        Frame.initialize();
        frmLogin.dispose();
      } else {
        JOptionPane.showMessageDialog(frmLogin, "Password incorrect.");
        passwordField.grabFocus();
        passwordField.setText("");
      }
      query.close();
    } catch (SQLException e) {
      System.out.println("Query failed.");
      JOptionPane.showMessageDialog(frmLogin,
          "Username/Password is not correct or does not exist.");
    } catch (HeadlessException e) {
      e.printStackTrace();
      System.out.println("Query failed.");
      JOptionPane.showMessageDialog(frmLogin,
          "Username/Password is not correct or does not exist.");
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Query failed.");
      JOptionPane.showMessageDialog(frmLogin,
          "Username/Password is not correct or does not exist.");
    }
    database.shutdown();
  }

  private static String hash(String salt, String input) throws Exception {
    try {
      byte[] pw;
      MessageDigest digest = MessageDigest.getInstance("SHA-256");
      pw = digest.digest((input + salt).getBytes("UTF-8"));
      return byteToHex(pw);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
      return null;
    }
  }

  public static String byteToHex(byte[] b) {
    String result = "";
    for (int i = 0; i < b.length; i++) {
      result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
    }
    return result;
  }
}
