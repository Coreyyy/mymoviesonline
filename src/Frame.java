import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;

public class Frame {

  private static JFrame frame;

  static String sqlAddress = SQL.sqlAddress;
  static String sqlTable = SQL.sqlTable;
  static String sqlUsername = SQL.sqlUsername;
  static String sqlPassword = SQL.sqlPassword;
  static String sqlDatabaseLocation = sqlAddress + "/" + sqlTable;

  static SQL database = new SQL(sqlUsername, sqlPassword, sqlDatabaseLocation, 1);
  private static JTextField txtUsername;
  private static JTextField txtPassword;
  private static JTextField txtEmail;
  private static JTextField txtEditUsername;
  private static JTextField txtEditPassword;
  private static JTextField txtEditEmail;

  private static int id = 0;
  private static int filmId = 0;
  private static JTextField txtTitle;
  private static JTextField txtYear;
  private static JTextField txtGenre;
  private static JTextField txtEditTitle;
  private static JTextField txtEditYear;
  private static JTextField txtEditGenre;
  private static JTextField txtDescription;
  private static JTextField txtDirector;
  private static JTextField txtEditDescription;
  private static JTextField txtEditActors;
  private static JTextField txtEditDirector;
  private static JTextField txtStock;
  private static JTextField txtEditPrice;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          @SuppressWarnings("unused")
          Frame window = new Frame();
          Frame.frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public Frame() {
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  static void initialize() {
    frame = new JFrame();
    frame.setVisible(true);
    frame.setBounds(100, 100, 450, 468);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().setLayout(null);

    JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
    tabbedPane.setBounds(0, 0, 432, 388);
    frame.getContentPane().add(tabbedPane);

    JPanel panel = new JPanel();
    tabbedPane.addTab("Films", null, panel, null);
    panel.setLayout(null);

    Panel panel_4 = new Panel();
    panel_4.setLayout(null);
    panel_4.setForeground(new Color(102, 204, 255));
    panel_4.setBounds(10, 10, 198, 215);
    panel.add(panel_4);

    JLabel lblNewFilm = new JLabel("New Film:");
    lblNewFilm.setFont(new Font("Tahoma", Font.PLAIN, 17));
    lblNewFilm.setBounds(0, 0, 82, 21);
    panel_4.add(lblNewFilm);

    JScrollPane scrollPane_1 = new JScrollPane();
    scrollPane_1.setBounds(10, 236, 201, 89);
    panel.add(scrollPane_1);

    JList<String> filmList = new JList<String>();
    scrollPane_1.setViewportView(filmList);

    JButton button = new JButton("Upload");
    button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!txtTitle.getText().trim().equals("")) {
          database.initialise();
          try {
            Film film = new Film(txtTitle.getText(), Integer.parseInt(txtYear.getText()),
                txtDescription.getText(), txtGenre.getText(), "", txtDirector.getText(), 0);
            @SuppressWarnings("unused")
            ResultSet query = database.executeQuery(
                "INSERT INTO movies (title, year, description, genre, actor, director, rating) "
                    + "VALUES ('" + film.getTitle() + "', '" + film.getYear() + "', '"
                    + film.getDescription() + "', '" + film.getGenre() + "', '" + film.getActors()
                    + "', '" + film.getDirector() + "', '" + film.getRating() + "')");

            JOptionPane.showMessageDialog(frame, "Film successfully added to the database.");
            System.out.println("Film " + film.getTitle() + " successfully added to database.");
            txtTitle.setText("");
            txtYear.setText("");
            txtDescription.setText("");
            txtGenre.setText("");
            txtDirector.setText("");
            loadFilms(filmList, false);
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          database.shutdown();
        } else
          JOptionPane.showMessageDialog(frame, "Title cannot be blank.");
      }
    });
    button.setBounds(12, 189, 82, 25);
    panel_4.add(button);

    JLabel lblTitle = new JLabel("Title:");
    lblTitle.setBounds(33, 34, 30, 16);
    panel_4.add(lblTitle);

    txtTitle = new JTextField();
    txtTitle.setColumns(10);
    txtTitle.setBounds(70, 34, 116, 22);
    panel_4.add(txtTitle);

    txtYear = new JTextField();
    txtYear.setColumns(10);
    txtYear.setBounds(70, 66, 116, 22);
    panel_4.add(txtYear);

    JLabel lblYear = new JLabel("Year:");
    lblYear.setBounds(26, 66, 37, 16);
    panel_4.add(lblYear);

    txtGenre = new JTextField();
    txtGenre.setColumns(10);
    txtGenre.setBounds(70, 98, 116, 22);
    panel_4.add(txtGenre);

    JLabel lblGenre = new JLabel("Genre:");
    lblGenre.setBounds(26, 98, 39, 16);
    panel_4.add(lblGenre);

    JButton button_2 = new JButton("Clear");
    button_2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        txtTitle.setText("");
        txtYear.setText("");
        txtDescription.setText("");
        txtGenre.setText("");
        txtDirector.setText("");
      }
    });
    button_2.setBounds(104, 189, 82, 25);
    panel_4.add(button_2);

    txtDescription = new JTextField();
    txtDescription.setColumns(10);
    txtDescription.setBounds(70, 130, 116, 22);
    panel_4.add(txtDescription);

    JLabel lblDescription = new JLabel("Description:");
    lblDescription.setBounds(0, 130, 74, 16);
    panel_4.add(lblDescription);

    txtDirector = new JTextField();
    txtDirector.setColumns(10);
    txtDirector.setBounds(70, 162, 116, 22);
    panel_4.add(txtDirector);

    JLabel lblDirector = new JLabel("Director:");
    lblDirector.setBounds(10, 162, 55, 16);
    panel_4.add(lblDirector);

    Panel panel_5 = new Panel();
    panel_5.setLayout(null);
    panel_5.setForeground(new Color(102, 204, 255));
    panel_5.setBounds(219, 10, 198, 344);
    panel.add(panel_5);

    JLabel lblEditFilm = new JLabel("Edit Film:");
    lblEditFilm.setFont(new Font("Tahoma", Font.PLAIN, 17));
    lblEditFilm.setBounds(0, 0, 82, 21);
    panel_5.add(lblEditFilm);

    JRadioButton rdbtnIncreaseBy = new JRadioButton("Increase by");
    JRadioButton rdbtnSetTo = new JRadioButton("Set to");
    rdbtnSetTo.setSelected(true);

    JButton btnDeleteFilm = new JButton("Delete");

    JButton btnUpdateFilm = new JButton("Update");
    btnUpdateFilm.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int dialogResult =
            JOptionPane.showConfirmDialog(frame, "Are you sure you want to update the film?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
          boolean success = false;
          database.initialise();
          try {
            @SuppressWarnings("unused")
            ResultSet query = database.executeQuery("UPDATE movies SET title='"
                + txtEditTitle.getText() + "', year='" + Integer.parseInt(txtEditYear.getText())
                + "', description='" + txtEditDescription.getText() + "', genre='"
                + txtEditGenre.getText() + "', actor='" + txtEditActors.getText() + "', director='"
                + txtEditDirector.getText() + "', instock="
                + (rdbtnIncreaseBy.isSelected()
                    ? "instock + " + Integer.parseInt(txtStock.getText())
                    : Integer.parseInt(txtStock.getText()))
                + ", price=" + Double.parseDouble(txtEditPrice.getText()) + " WHERE ID=" + filmId + ";");
            success = true;
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          if (success) {
            Film f = Film.getFilm(filmId);
            txtEditTitle.setText("");
            txtEditYear.setText("");
            txtEditGenre.setText("");
            txtEditDescription.setText("");
            txtEditDirector.setText("");
            txtEditActors.setText("");
            txtStock.setText("");
            txtEditPrice.setText("");
            btnUpdateFilm.setEnabled(false);
            btnDeleteFilm.setEnabled(false);
            updatingFilm(false);
            loadFilms(filmList, false);
            JOptionPane.showMessageDialog(frame, "Film  '" + f.getTitle() + "' updated.");
          } else {
            JOptionPane.showMessageDialog(frame, "Failed to update film.", "Query Failed",
                JOptionPane.ERROR_MESSAGE);
          }
          filmId = 0;
          database.shutdown();
        }

      }
    });
    btnUpdateFilm.setEnabled(false);
    btnUpdateFilm.setBounds(12, 306, 82, 25);
    panel_5.add(btnUpdateFilm);

    JLabel lblTitle_1 = new JLabel("Title:");
    lblTitle_1.setBounds(27, 37, 36, 16);
    panel_5.add(lblTitle_1);

    JLabel lblGenre_1 = new JLabel("Genre:");
    lblGenre_1.setBounds(24, 98, 39, 16);
    panel_5.add(lblGenre_1);

    txtEditTitle = new JTextField();
    txtEditTitle.setEnabled(false);
    txtEditTitle.setColumns(10);
    txtEditTitle.setBounds(70, 34, 116, 22);
    panel_5.add(txtEditTitle);

    txtEditYear = new JTextField();
    txtEditYear.setEnabled(false);
    txtEditYear.setColumns(10);
    txtEditYear.setBounds(70, 66, 116, 22);
    panel_5.add(txtEditYear);

    JLabel lblYear_1 = new JLabel("Year:");
    lblYear_1.setBounds(27, 69, 36, 16);
    panel_5.add(lblYear_1);

    txtEditGenre = new JTextField();
    txtEditGenre.setEnabled(false);
    txtEditGenre.setColumns(10);
    txtEditGenre.setBounds(70, 98, 116, 22);
    panel_5.add(txtEditGenre);

    btnDeleteFilm.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int dialogResult =
            JOptionPane.showConfirmDialog(frame, "Are you sure you want to delete this film?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
          boolean success = false;
          database.initialise();
          try {
            @SuppressWarnings("unused")
            ResultSet query = database.executeQuery("DELETE FROM movies WHERE ID=" + filmId + ";");
            success = true;
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          if (success) {
            Film f = Film.getFilm(filmId);
            txtEditTitle.setText("");
            txtEditYear.setText("");
            txtEditGenre.setText("");
            txtEditDescription.setText("");
            txtEditDirector.setText("");
            txtEditActors.setText("");
            txtStock.setText("");
            txtEditPrice.setText("");
            btnUpdateFilm.setEnabled(false);
            btnDeleteFilm.setEnabled(false);
            updatingFilm(false);
            loadFilms(filmList, false);
            JOptionPane.showMessageDialog(frame, "Film  '" + f.getTitle() + "' deleted.");
          } else {
            JOptionPane.showMessageDialog(frame, "Failed to delete film.", "Query Failed",
                JOptionPane.ERROR_MESSAGE);
          }
          filmId = 0;
          database.shutdown();
        }
      }
    });
    btnDeleteFilm.setEnabled(false);
    btnDeleteFilm.setBounds(104, 306, 82, 25);
    panel_5.add(btnDeleteFilm);

    txtEditDescription = new JTextField();
    txtEditDescription.setEnabled(false);
    txtEditDescription.setColumns(10);
    txtEditDescription.setBounds(70, 130, 116, 22);
    panel_5.add(txtEditDescription);

    JLabel lblDescription_1 = new JLabel("Description:");
    lblDescription_1.setBounds(0, 130, 82, 16);
    panel_5.add(lblDescription_1);

    txtEditActors = new JTextField();
    txtEditActors.setEnabled(false);
    txtEditActors.setColumns(10);
    txtEditActors.setBounds(70, 194, 116, 22);
    panel_5.add(txtEditActors);

    JLabel lblActors = new JLabel("Actors:");
    lblActors.setBounds(22, 194, 41, 16);
    panel_5.add(lblActors);

    txtEditDirector = new JTextField();
    txtEditDirector.setEnabled(false);
    txtEditDirector.setColumns(10);
    txtEditDirector.setBounds(70, 162, 116, 22);
    panel_5.add(txtEditDirector);

    JLabel lblDirector_1 = new JLabel("Director:");
    lblDirector_1.setBounds(10, 162, 51, 16);
    panel_5.add(lblDirector_1);

    rdbtnSetTo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        rdbtnIncreaseBy.setSelected(!rdbtnIncreaseBy.isSelected());
      }
    });
    rdbtnIncreaseBy.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        rdbtnSetTo.setSelected(!rdbtnSetTo.isSelected());
      }
    });
    rdbtnIncreaseBy.setBounds(91, 254, 95, 25);
    panel_5.add(rdbtnIncreaseBy);

    rdbtnSetTo.setBounds(91, 279, 95, 25);
    panel_5.add(rdbtnSetTo);

    JLabel lblStockLevel = new JLabel("Stock Level:");
    lblStockLevel.setBounds(12, 257, 70, 16);
    panel_5.add(lblStockLevel);

    txtStock = new JTextField();
    txtStock.setEnabled(false);
    txtStock.setBounds(12, 280, 70, 22);
    panel_5.add(txtStock);
    txtStock.setColumns(10);

    txtEditPrice = new JTextField();
    txtEditPrice.setEnabled(false);
    txtEditPrice.setColumns(10);
    txtEditPrice.setBounds(70, 226, 116, 22);
    panel_5.add(txtEditPrice);

    JLabel lblPrice = new JLabel("Price (\u00A3):");
    lblPrice.setBounds(12, 226, 55, 16);
    panel_5.add(lblPrice);

    JButton btnLoadFilms = new JButton("Load Films");
    btnLoadFilms.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        loadFilms(filmList, true);
      }
    });
    btnLoadFilms.setBounds(10, 329, 99, 25);
    panel.add(btnLoadFilms);

    JButton button_8 = new JButton("Edit");
    button_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        String film[] = filmList.getSelectedValue().split("-");
        Film f = Film.getFilm(film[0]);
        updatingFilm(true);
        btnUpdateFilm.setEnabled(true);
        btnDeleteFilm.setEnabled(true);
        txtEditTitle.setText(f.getTitle());
        txtEditYear.setText(Integer.toString(f.getYear()));
        txtEditGenre.setText(f.getGenre());
        txtEditDescription.setText(f.getDescription());
        txtEditDirector.setText(f.getDirector());
        txtEditActors.setText(f.getActors());
        txtStock.setText(Integer.toString(f.getStock()));
        txtEditPrice.setText(Double.toString(f.getPrice()));
        filmId = f.getId();
      }
    });
    button_8.setBounds(121, 329, 90, 25);
    panel.add(button_8);

    JSeparator separator_2 = new JSeparator();
    separator_2.setOrientation(SwingConstants.VERTICAL);
    separator_2.setBounds(210, 10, 2, 222);
    panel.add(separator_2);

    JSeparator separator_3 = new JSeparator();
    separator_3.setBounds(10, 231, 200, 2);
    panel.add(separator_3);

    JPanel panel_1 = new JPanel();
    tabbedPane.addTab("Users", null, panel_1, null);
    panel_1.setLayout(null);

    Panel panel_2 = new Panel();
    panel_2.setForeground(new Color(102, 204, 255));
    panel_2.setBounds(10, 10, 198, 215);
    panel_1.add(panel_2);
    panel_2.setLayout(null);

    JLabel lblNewLabel = new JLabel("New User:");
    lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
    lblNewLabel.setBounds(0, 0, 82, 21);
    panel_2.add(lblNewLabel);

    JCheckBox chkEmail = new JCheckBox("");
    chkEmail.setBounds(67, 157, 24, 22);
    chkEmail.setSelected(true);
    panel_2.add(chkEmail);

    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setBounds(10, 236, 201, 89);
    panel_1.add(scrollPane);

    JList<String> list = new JList<String>();
    scrollPane.setViewportView(list);

    JButton btnUpload = new JButton("Upload");
    btnUpload.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!txtUsername.getText().trim().equals("") && !txtPassword.getText().trim().equals("")
            && !txtEmail.getText().trim().equals("")) {
          database.initialise();
          try {
            String salt = User.byteToHex(User.getNextSalt());
            User user = new User(txtUsername.getText(), User.hash(txtPassword.getText(), salt),
                salt, txtEmail.getText());
            @SuppressWarnings("unused")
            ResultSet query =
                database.executeQuery("INSERT INTO users (username, password, salt, email) "
                    + "VALUES ('" + user.getUsername() + "', '" + user.getPassword() + "', '"
                    + user.getSalt() + "', '" + user.getEmail() + "')");

            if (chkEmail.isSelected()) {
              Email.send(user);
            }
            JOptionPane.showMessageDialog(frame, "User successfully added to the database.");
            System.out.println("User " + user.getUsername() + " successfully added to database.");
            txtUsername.setText("");
            txtPassword.setText("");
            txtEmail.setText("");
            chkEmail.setSelected(false);
            loadUsers(list, false);
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          database.shutdown();
        } else
          JOptionPane.showMessageDialog(frame, "Username, Password or Email cannot be blank.");
      }
    });
    btnUpload.setBounds(12, 189, 82, 25);
    panel_2.add(btnUpload);

    JLabel lblUsername = new JLabel("Username:");
    lblUsername.setBounds(0, 37, 63, 16);
    panel_2.add(lblUsername);

    txtUsername = new JTextField();
    txtUsername.setBounds(70, 34, 116, 22);
    panel_2.add(txtUsername);
    txtUsername.setColumns(10);

    txtPassword = new JTextField();
    txtPassword.setColumns(10);
    txtPassword.setBounds(70, 66, 116, 22);
    panel_2.add(txtPassword);

    JLabel lblPassword = new JLabel("Password:");
    lblPassword.setBounds(0, 66, 63, 16);
    panel_2.add(lblPassword);

    txtEmail = new JTextField();
    txtEmail.setColumns(10);
    txtEmail.setBounds(70, 127, 116, 22);
    panel_2.add(txtEmail);

    JLabel lblEmail = new JLabel("Email:");
    lblEmail.setBounds(27, 130, 36, 16);
    panel_2.add(lblEmail);

    Panel panel_3 = new Panel();
    panel_3.setLayout(null);
    panel_3.setForeground(new Color(102, 204, 255));
    panel_3.setBounds(219, 10, 198, 344);
    panel_1.add(panel_3);

    JLabel lblEditUser = new JLabel("Edit User:");
    lblEditUser.setFont(new Font("Tahoma", Font.PLAIN, 17));
    lblEditUser.setBounds(0, 0, 82, 21);
    panel_3.add(lblEditUser);

    JCheckBox chkAdmin = new JCheckBox("");
    JButton btnDelete = new JButton("Delete");
    JButton btnEditGenerate = new JButton("Generate");
    JButton btnDisable = new JButton("Disable");

    JButton btnUpdate = new JButton("Update");
    btnUpdate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int dialogResult =
            JOptionPane.showConfirmDialog(frame, "Are you sure you want to update the user?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
          boolean success = false;
          database.initialise();
          try {
            User u = User.getUser(id);
            @SuppressWarnings("unused")
            ResultSet query = database.executeQuery("UPDATE users SET username='"
                + txtEditUsername.getText() + "', email='" + txtEditEmail.getText() + "', admin='"
                + (chkAdmin.isSelected() == true ? 1 : 0) + "', password='"
                + (txtEditPassword.getText().trim().equals("") ? u.getPassword()
                    : User.hash(txtEditPassword.getText(), u.getSalt()))
                + "' WHERE ID=" + id + ";");
            success = true;
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          if (success) {
            User u = User.getUser(id);
            txtEditUsername.setText("");
            txtEditPassword.setText("");
            txtEditEmail.setText("");
            btnUpdate.setEnabled(false);
            btnDisable.setEnabled(false);
            btnDelete.setEnabled(false);
            btnEditGenerate.setEnabled(false);
            chkAdmin.setEnabled(false);
            updatingUser(false);
            loadUsers(list, false);
            JOptionPane.showMessageDialog(frame, "User  '" + u.getUsername() + "' updated.");
          } else {
            JOptionPane.showMessageDialog(frame, "Failed to update user.", "Query Failed",
                JOptionPane.ERROR_MESSAGE);
          }
          id = 0;
          database.shutdown();
        }
      }
    });
    btnUpdate.setEnabled(false);
    btnUpdate.setBounds(12, 275, 82, 25);
    panel_3.add(btnUpdate);

    JLabel label_1 = new JLabel("Username:");
    label_1.setBounds(0, 37, 63, 16);
    panel_3.add(label_1);

    JLabel label_3 = new JLabel("Email:");
    label_3.setBounds(27, 130, 36, 16);
    panel_3.add(label_3);

    btnEditGenerate.setEnabled(false);
    btnEditGenerate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        txtEditPassword.setText(User.generatePassword());
      }
    });
    btnEditGenerate.setBounds(70, 92, 116, 25);
    panel_3.add(btnEditGenerate);

    btnDisable.setEnabled(false);
    btnDisable.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int dialogResult =
            JOptionPane.showConfirmDialog(frame, "Are you sure you want to disable the user?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
          boolean success = false;
          database.initialise();
          try {
            @SuppressWarnings("unused")
            ResultSet query =
                database.executeQuery("UPDATE users SET password='', salt='' WHERE ID=" + id + ";");
            success = true;
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          if (success) {
            User u = User.getUser(id);
            txtEditUsername.setText("");
            txtEditPassword.setText("");
            txtEditEmail.setText("");
            btnUpdate.setEnabled(false);
            btnDisable.setEnabled(false);
            btnDelete.setEnabled(false);
            btnEditGenerate.setEnabled(false);
            chkAdmin.setEnabled(false);
            updatingUser(false);
            loadUsers(list, false);
            JOptionPane.showMessageDialog(frame, "User  '" + u.getUsername() + "' deleted.");
          } else {
            JOptionPane.showMessageDialog(frame, "Failed to disable user.", "Query Failed",
                JOptionPane.ERROR_MESSAGE);
          }
          id = 0;
          database.shutdown();
        }
      }
    });
    btnDisable.setBounds(12, 306, 82, 25);
    panel_3.add(btnDisable);

    txtEditUsername = new JTextField();
    txtEditUsername.setEnabled(false);
    txtEditUsername.setColumns(10);
    txtEditUsername.setBounds(70, 34, 116, 22);
    panel_3.add(txtEditUsername);

    txtEditPassword = new JTextField();
    txtEditPassword.setEnabled(false);
    txtEditPassword.setColumns(10);
    txtEditPassword.setBounds(70, 66, 116, 22);
    panel_3.add(txtEditPassword);

    JLabel label_2 = new JLabel("Password:");
    label_2.setBounds(0, 66, 63, 16);
    panel_3.add(label_2);

    txtEditEmail = new JTextField();
    txtEditEmail.setEnabled(false);
    txtEditEmail.setColumns(10);
    txtEditEmail.setBounds(70, 127, 116, 22);
    panel_3.add(txtEditEmail);

    btnDelete.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int dialogResult =
            JOptionPane.showConfirmDialog(frame, "Are you sure you want to delete the user?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (dialogResult == 0) {
          boolean success = false;
          database.initialise();
          try {
            @SuppressWarnings("unused")
            ResultSet query = database.executeQuery("DELETE FROM users WHERE ID=" + id + ";");
            success = true;
          } catch (HeadlessException E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          } catch (Exception E) {
            E.printStackTrace();
            System.out.println("Query failed.");
          }
          if (success) {
            User u = User.getUser(id);
            txtEditUsername.setText("");
            txtEditPassword.setText("");
            txtEditEmail.setText("");
            btnUpdate.setEnabled(false);
            btnDisable.setEnabled(false);
            btnDelete.setEnabled(false);
            btnEditGenerate.setEnabled(false);
            chkAdmin.setEnabled(false);
            updatingUser(false);
            loadUsers(list, false);
            JOptionPane.showMessageDialog(frame, "User  '" + u.getUsername() + "' deleted.");
          } else {
            JOptionPane.showMessageDialog(frame, "Failed to delete user.", "Query Failed",
                JOptionPane.ERROR_MESSAGE);
          }
          id = 0;
          database.shutdown();
        }
      }
    });
    btnDelete.setEnabled(false);
    btnDelete.setBounds(104, 306, 82, 25);
    panel_3.add(btnDelete);

    chkAdmin.setEnabled(false);
    chkAdmin.setBounds(67, 158, 24, 22);
    panel_3.add(chkAdmin);

    JLabel lblAdmin = new JLabel("Admin:");
    lblAdmin.setBounds(22, 159, 41, 16);
    panel_3.add(lblAdmin);

    JButton btnGenerate = new JButton("Generate");
    btnGenerate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        txtPassword.setText(User.generatePassword());
      }
    });
    btnGenerate.setBounds(70, 90, 116, 25);
    panel_2.add(btnGenerate);

    JLabel lblSendEmail = new JLabel("Send Email:");
    lblSendEmail.setBounds(0, 160, 70, 16);
    panel_2.add(lblSendEmail);

    JButton btnClear = new JButton("Clear");
    btnClear.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        txtUsername.setText("");
        txtPassword.setText("");
        txtEmail.setText("");
        chkEmail.setSelected(false);;
      }
    });
    btnClear.setBounds(104, 189, 82, 25);
    panel_2.add(btnClear);

    JButton btnLoadUsers = new JButton("Load Users");
    btnLoadUsers.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        loadUsers(list, true);
      }
    });
    btnLoadUsers.setBounds(10, 329, 99, 25);
    panel_1.add(btnLoadUsers);

    JButton btnEdit = new JButton("Edit");
    btnEdit.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        String user[] = list.getSelectedValue().split("-");
        User u = User.getUser(user[0]);
        updatingUser(true);
        btnUpdate.setEnabled(true);
        btnDisable.setEnabled(true);
        btnDelete.setEnabled(true);
        btnEditGenerate.setEnabled(true);
        chkAdmin.setEnabled(true);
        txtEditUsername.setText(u.getUsername());
        txtEditEmail.setText(u.getEmail());
        chkAdmin.setSelected(u.getAdmin());
        id = u.getId();
      }
    });
    btnEdit.setBounds(121, 329, 90, 25);
    panel_1.add(btnEdit);

    JSeparator separator = new JSeparator();
    separator.setOrientation(SwingConstants.VERTICAL);
    separator.setBounds(210, 10, 2, 222);
    panel_1.add(separator);

    JSeparator separator_1 = new JSeparator();
    separator_1.setBounds(10, 231, 200, 2);
    panel_1.add(separator_1);

    JButton btnLogout = new JButton("Log Out");
    btnLogout.setBounds(349, 390, 83, 31);
    btnLogout.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {

        JOptionPane.showMessageDialog(frame, "Logout successful");
        Login.initialize();
        frame.dispose();

      }
    });
    frame.getContentPane().add(btnLogout);
    frame.setTitle("My Movies Online");

  }

  private static void loadUsers(JList<String> list, boolean notify) {
    try {
      User.retrieveUsers(frame, notify);
      String[] users = new String[User.userList.size()];
      String temp = "";
      for (int i = 0; i < User.userList.size(); i++) {
        if (!User.userList.get(i).getUsername().trim().equals("")) {
          users[i] = String.valueOf(User.userList.get(i).getId()) + " - "
              + User.userList.get(i).getUsername()
              + (User.userList.get(i).getAdmin() ? " - Admin" : "")
              + (User.userList.get(i).getSalt().trim().equals("")
                  && User.userList.get(i).getPassword().trim().equals("") ? " - Disabled" : "");
        }
      }
      /*
       * Bubble Sort sorting Users in ID order
       */
      boolean flag = true;

      while (flag) {
        flag = false;
        for (int j = 0; j < users.length - 1; j++) {
          String[] temp2 = users[j].split("-");
          String[] temp3 = users[j + 1].split("-");
          if (Integer.parseInt(temp2[0].trim()) > Integer.parseInt(temp3[0].trim())) {
            temp = users[j];
            users[j] = users[j + 1];
            users[j + 1] = temp;
            flag = true;
          }
        }
      }
      list.setListData(users);
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
  }

  private static void loadFilms(JList<String> list, boolean notify) {
    try {
      Film.retrieveFilms(frame, notify);
      String[] films = new String[Film.filmList.size()];
      String temp = "";
      for (int i = 0; i < Film.filmList.size(); i++) {
        // if (!Film.filmList.get(i).getTitle().trim().equals("")) {
        films[i] = String.valueOf(Film.filmList.get(i).getId()) + " - "
            + Film.filmList.get(i).getTitle() + " - " + Film.filmList.get(i).getYear() + " - "
            + Film.filmList.get(i).getStock() + " in stock";
        // }
      }
      /*
       * Bubble Sort sorting Films in ID order
       */
      boolean flag = true;

      while (flag) {
        flag = false;
        for (int j = 0; j < films.length - 1; j++) {
          String[] temp4 = films[j].split("-");
          String[] temp5 = films[j + 1].split("-");
          if (Integer.parseInt(temp4[0].trim()) > Integer.parseInt(temp5[0].trim())) {
            temp = films[j];
            films[j] = films[j + 1];
            films[j + 1] = temp;
            flag = true;
          }
        }
      }
      list.setListData(films);
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
  }

  private static void updatingUser(boolean updating) {
    txtEditUsername.setEnabled(updating);
    txtEditPassword.setEnabled(updating);
    txtEditEmail.setEnabled(updating);
  }

  private static void updatingFilm(boolean updating) {
    txtEditTitle.setEnabled(updating);
    txtEditYear.setEnabled(updating);
    txtEditGenre.setEnabled(updating);
    txtEditDescription.setEnabled(updating);
    txtEditDirector.setEnabled(updating);
    txtEditActors.setEnabled(updating);
    txtEditPrice.setEnabled(updating);
    txtStock.setEnabled(updating);
  }
}
